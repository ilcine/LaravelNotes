
# db e veri üretir.

# database\factories\CrudsFactory.php  # yarat

```
$factory->define(App\Crud::class, function (Faker $faker) {
    return [
        'urunAdi' => $faker->name,
        'urunFiyati' => $faker->randomFloat(2,1,100)
    ];
});
```

## database/seeds/DatabaseSeeder.php

10 kayıt olsun

```
    public function run()
    {
				factory(\App\Crud::class,10)->create();
    }
```

## `php artisan db:seed`


kayıtları db e yaz.
