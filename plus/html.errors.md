﻿```
200: TAMAM. Standart başarı kodu ve varsayılan seçenek.
201: Nesne oluşturuldu. storeEylemler için kullanışlıdır.
204: İçerik yok. Bir işlem başarılı bir şekilde yürütüldüğünde, ancak iade edilecek içerik yoktur.
206: Kısmi içerik. Bir paginated kaynak listesini döndürmek gerektiğinde kullanışlıdır.
400: Geçersiz istek. Doğrulama geçemeyen istekler için standart seçenek.
401: Yetkisiz. Kullanıcının doğrulanması gerekiyor.
403: Yasak. Kullanıcı kimliği doğrulandı, ancak bir eylemi gerçekleştirmek için gerekli izinlere sahip değil.
404: Bulunamadı. Kaynak bulunmadığında bu, Laravel tarafından otomatik olarak geri döndürecektir.
500: İç Sunucu Hatası. İdeal olarak bunu açıkça iade etmeyeceksiniz, ancak beklenmedik bir şey olursa, bu kullanıcının alacağı şeydir.
503: Hizmet kullanılamıyor. Oldukça açıklayıcı, ama aynı zamanda uygulama tarafından açıkça iade edilmeyecek başka bir kod.
```

```
200: OK. The standard success code and default option.
201: Object created. Useful for the store actions.
204: No content. When an action was executed successfully, but there is no content to return.
206: Partial content. Useful when you have to return a paginated list of resources.
400: Bad request. The standard option for requests that fail to pass validation.
401: Unauthorized. The user needs to be authenticated.
403: Forbidden. The user is authenticated, but does not have the permissions to perform an action.
404: Not found. This will be returned automatically by Laravel when the resource is not found.
500: Internal server error. Ideally you're not going to be explicitly returning this, but if something unexpected breaks, this is what your user is going to receive.
503: Service unavailable. Pretty self explanatory, but also another code that is not going to be returned explicitly by the application.
```
