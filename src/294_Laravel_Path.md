# Laravel Path

_Emrullah İLÇİN_

## Requirment
    
    Ubuntu 16.04
    Larvel 5.6
    php 7.2

## Examples

`cd $HOME/{ laravel project name }/`  # for example: `/home/emr/project1/`

1) `__DIR__`   # display BASE_DIR `/home/emr/project1/`

2) `__DIR__ . "/public"`  #  display `/home/emr/project1/public`

3) `base_path()`  # display BASE_DIR `/home/emr/project1/`

3) `base_path('.env')` #  display `/home/emr/project1/.env`

4) `app()->environmentFilePath()`   # display `/home/emr/project1/.env`

5) `public_path()`  # display `/home/emr/project1/public`

6)  `public_path('auth')`  # display `/home/emr/project1/public/auth`

7)  `config_path()`  # display `/home/emr/project1/config`

8)  `resource_path('js/1')`  # display `/home/emr/project1/resource/js/1`

9) and `app_path()`, `database_path()`, `mix()`, `storage_path()`

10) `getenv('HOME')` or `$_SERVER["HOME"]`  # display `/home/emr`
