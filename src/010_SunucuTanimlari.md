# Server definitions for Laravel

_Emrullah İLÇİN_

## Tested environment

    Ubuntu 16.04
    Larvel 5.6
    Apache2 2.4.18 
    php 7.2 

* [Composer install on Ubuntu:](https://gitlab.com/ilcine/UbuntuNotes/blob/master/src/800_Composer.md)
* [NPM install on Ubuntu:](https://gitlab.com/ilcine/UbuntuNotes/blob/master/src/810_NPM.md)
* [Install PHP on Ubuntu and define in apache2:](https://gitlab.com/ilcine/UbuntuNotes/blob/master/src/820_Install_php.md)
* [Install Apache and Virtual Host on Ubuntu:](https://gitlab.com/ilcine/UbuntuNotes/blob/master/src/830_ApacheVirtualHost.md)
* [Install MySql on Ubuntu:](https://gitlab.com/ilcine/UbuntuNotes/blob/master/src/840_mysql.md)
* [Install PhpMyAdmin on Ubuntu:](https://gitlab.com/ilcine/UbuntuNotes/blob/master/src/850_phpMyAdmin.md)

## You can also install it as Homestead

[Homestead Install on Windows](https://gitlab.com/ilcine/LaravelNotes/-/blob/master/src/011_Homestead.installation.md)
