# DB examples


## 1. Model query in model

```
$model = new Emr\Vpv\ApiTest; 
$model = $model->all();  
//return $model; // output full 

$model = $model->where('product_price',1);
//dd($model); //ok output: 0=>ApiTest#Original:array:5["id=>1]
//return $model; //ok   output: { {}{} }
return response()->json($model);  // output: [ {} {} ]
```

## 2. If controller
`return DB::table('api_test')->whereRaw('product_price = IF(id > 2, 1, 2)')->get();`

## 3. Conditional Clauses (Şart çümlecikleri)

```
$role = "1";
return $model = DB::table('api_test')
		->when($role, function ($query, $role) {
		return $query->where('product_price', $role);
		})->get();
```	

##  4. Eloquent: Relationships

in model
```
class PrUsers extends Model
{
    public function UserX()
    {		
        // -----------------[     User     ]--[  PrUsers   ] // for hasOne and hasMany  
        return $this->hasOne('App\User', 'id', 'pr_user_id' ) // ok
        // return $this->hasMany('App\User', 'id', 'pr_user_id' ) //ok
        // return $this->belongsTo('App\User', 'pr_user_id', 'id') // ok reverse
    		->join('pr_users', 'pr_users.pr_user_id', '=', 'users.id')
    		   //->Select(\DB::raw('pr_users.*,users.*')) // ok other select ex;
    		->SelectRaw('users.*,pr_users.*')
    		->get();
    	// not: controller use:	"$a = PrUsers::findOrFail(1)->UserX();"
    }
}    
```

in Controller or route
```
Route::get('/test2', function () {
  //$aa = PrUsers::where('pr_user_id',1)->first(); $aa = $aa->UserX(); // ok
  $aa = PrUsers::where('pr_user_id',2)->firstOrFail(); $aa = $aa->UserX(); // ok
  //$aa = PrUsers::findOrFail(2)->UserX(); // ok; 
  //$aa = PrUsers::findOrFail(1); $aa = $aa->UserX();  //ok
  return response()->json($aa);
});
```

Output: `display Users and PrUsers table with join`
 
## Migration Structure

ex1: create  table
```
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightsTable extends Migration
{
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
        });
    }
    public function down()
    {
        Schema::drop('flights');
    }
}
```


ex2: add or delete table
```
public function up()
	{
		Schema::table('users', function (Blueprint $table) {
		. 
		.
	 });
public function down()
	{
		if (Schema::hasTable('users')) {
			Schema::table('users', function (Blueprint $table) {
			  //$table->dropForeign(['layout_id']);
			  $table->dropColumn('api_token');
			});
		}	
	}	 
	 
```



	} 
referance: ex
```
$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade'); // onDelete yazarsan users silinirse otomatikman buda silinir.
$table->foreign('user_id')->references('id')->on('users'); // bu kayıt silinmeden users deki kayıt silinmez.
```


##  5. Eloquent: with and Relationships

web.php

`Route::get('test', 'MessagesController@test');`

Model with User.php

```
class User extends Authenticatable
{
public function detail()
    {
        return $this->hasMany(UserDetail::class, 'user_id')
            ->select('users_detail.id','user_id','role')
			->where('users_detail.id',1);
    }
}
```

UserDetail stracture in database directory

```
class CreateUsersDetailTable extends Migration
{
	public function up()
	{
		Schema::create('users_detail', function (Blueprint $table) {
			$table->bigIncrements('id');					 
			$table->unsignedBigInteger('user_id')->nullable();
			$table->integer('role')->default(1);  	
		});
	}
}	
```

Controller
```
class MessagesController extends Controller
{

  // ATTENTION: use "with" commonds with id and user_id;  // "Without this, laravel IDs cannot match"


	// with auth ex:1 //  result: [{"id":1,"user_id":1,"role":6}] // get auth id
	return auth()->user()->detail()->get(); 

    result: [{"id":1,"detail":[{"id":1,"user_id":1,"role":6}]}]
    return auth()->user()->where('id',1)->select('users.id')->with('detail')->get();


	// with auth and use id=2 ex:2	// result: //  [{"id":1,"role":6}]
	return auth()->user()->query()->where('id',1)->with(['detail' => function ($query) {
		$query->where('users_detail.id',1);
		}])->get();	 

    result: // [{"id":1,"detail":[{"id":1,"user_id":1,"role":6}]}]
    return User::where('users.id','1')->select('id')->with('detail')->get();


	// withouth auth ex:3 // result: [{"id":1,"name":"admin","detail":[{"id":1,"user_id":1,"role":6}]}]
	$u2= User::query();
	$u2->where("users.id",request()->sender_id);
	$u2->select('id','name');
	//$u->get(); // not required
	//$u2->with('detail');   //or use the following "with" command
	$u2->with(['detail' => function ($query) {
		$query->where('users_detail.id',request()->input('sender_id'));
		$query->select('users_detail.id','user_id','role');
	}]);
	return $u2->get();


	// withouth auth ex:4 // result: `[{"id":1,"name":"admin","detail":[{"id":1,"user_id":1,"role":6}]}]`
	return User::query()->where('id',1)->with(['detail' => function ($query) {
		$query->where('users_detail.id',1);
		}])->get();	
		
	
	
}
```

//--------------------------------------------------------------//

// or use models protect `$with`
```
protected $with = ['detail'];
	public function detail()
    {
        return $this->hasMany('App\UserDetail', 'user_id')
				->select('users_detail.id','user_id','role')
				->where('users_detail.id',1);
    }
```    
// controllers

return User::where('users.id','1')->select('id')->get();

//result: `[{"id":1,"detail":[{"id":1,"user_id":1,"role":6}]}]`

//-----------------------------------------------------//


use

`http://proje1.ilcin.name.tr/test2?sender_id=1`



