# How to Use Laravel Config Files

_Emrullah İLÇİN_

## 1 Requirment
    
    Ubuntu 16.04
    Larvel 5.6

## 2 create config file  

ex: `edit $HOME/{laravel project name}/config/emrconfig.php`

```php
<?php
return [
    'emr_client' => [
        'id' => env('EMR_CLIENT_ID'),
        'secret' => env('EMR_CLIENT_SECRET'),
    ], 
];
```

## 3 add .env 

```
EMR_CLIENT_ID=2
EMR_CLIENT_SECRET=12345678
```

### 4 use ex:1

```
<?php

namespace App\Providers;

use Config;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
         Config::get('emrconfig.emr_client.id');
         Config::get('emrconfig.emr_client.secret');
    }
}
```

> To enable dynamic `Config::set('emrconfig.emr_client.id','3');`


## 5 use ex:2

route::get('/emrconfig', function () {
	echo Config::get('emrconfig.emr_client.id') . "\n";
});

> Don't forget to `php artisan config:cache` to commit config changes.


