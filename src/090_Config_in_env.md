# Sample ".env" file

_Emrullah İLÇİN_

| Tanım                 | Açıklama                                                               |
|-----------------------|------------------------------------------------------------------------|
|	APP_NAME=Proje1  	|	Proje adı girilir	|
|	APP_ENV=local	    |	env in yolu	        |
|	APP_KEY=base64:xxxxxxxxxxxxxxxxxxxxxxx	|	Her proje için laravel in atadığı key, gerekiyorsa biz yeniden oluşturabiliriz.	|
|	APP_DEBUG=true	    |	Debug(hata kontrol ve izleme) durumu, proje tamamlandığında false yapılabilinir	|
|	APP_URL=http://proje1.ilcin.name.tr	|	Web sayfasının url si yazılır	|
|	LOG_CHANNEL=stack	|	Loglar nasıl duracak	|
|	DB_CONNECTION=mysql	|	DB olarak MySql kullanımı ( "PostgreSQL","SQLite","SQL Server" DB lerde kullanılır) 	|
|	DB_HOST=127.0.0.1	|	DB consoluna erişim local kalabilir.	|
|	DB_PORT=3306	    |	MySql in standart portu 3306 (PostgreSQL:5432,SQL Server:1433,SQLite:none)	|
|	DB_DATABASE=proje1	|	Kullanıcı için MySql de açılan DB nin adı biz proje1 demiştik	|
|	DB_USERNAME=emr	    |	MySql deki kullanıcının user name i biz emr demiştik	|
|	DB_PASSWORD=123456	|	MySql deki kullanıcını şifresi biz 123456 demiştik (Gerçek projede daha güvenli şifre kullanın) 	|
|	BROADCAST_DRIVER=log	|	Log lar hangi sürücüyü kullanacak	|
|	CACHE_DRIVER=file	|	Cache ler dosya da tutulacak	|
|	SESSION_DRIVER=file	|	Session'ları dosyada tutar, sesionlar DB de tutulsun isterseniz config/session.php da detaylı tanım ve DB te desen tanımı yapılır	|
|	SESSION_LIFETIME=120	|	Session süresi 120 dk	|
|	QUEUE_DRIVER=sync	|	Kuyruk sürücü tanımı	|
|	REDIS_HOST=127.0.0.1	|	Redis kullanacaksak hostu	|
|	REDIS_PASSWORD=null	|	Redis şifresi	|
|	REDIS_PORT=6379  	|	Redis in portu	|
|	MAIL_DRIVER=smtp	|	mail gönderme protokolu seçilir.	|
|	MAIL_HOST=smtp.mailtrap.io	|	Bu sistem mail suncu olmayacak, mail i mailtrap.io sitesi üzerinden atacaksa bu tanımı kullan (başka mail hosting i de kullanılır); mail leri bu sistem atacaksa DNS te smtp tanımı yapıldıktan sonra smtp.ilcin.name.tr yaz. 	|
|	MAIL_PORT=2525	   |	Post edilecek maillerin port nosu 	|
|	MAIL_USERNAME=null	|	Mail gönderecek kullanıcının user i	|
|	MAIL_PASSWORD=null	|	Mail gönderecek kullanıcını şifresi	|
|	MAIL_ENCRYPTION=null	|	Mail enc yapılacakmı yapılacaksa enc seçilir.	|
|	PUSHER_APP_ID=	  |	Gerekiyorsa 	|
|	PUSHER_APP_KEY=	   |		|
|	PUSHER_APP_SECRET=	|		|
|	PUSHER_APP_CLUSTER=mt1	|		|
|	MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"	|		|
|	MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"	|		|

