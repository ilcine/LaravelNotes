# Laravel invite

Davet usulu login işlemi

_Emrullah İLÇİN_

## migration ile tablo dosyasını yarat ve editle 

`php artisan make:migration create_invites_table`
"database/migrations/*_create_invites_table.php"

```php
 public function up()
  {
    Schema::create('invites', function (Blueprint $table) {
        $table->increments('id');
        $table->string('email');
        $table->string('token', 16)->unique();
        $table->timestamps();
    });
  }

  public function down()
  {
     Schema::dropIfExists('invites');
  }
```

`php artisan migrate`  // sql e yazar, tablo oluşur

## model yarat ve editle 

`php artisan make:model Invite`
"app/Invite.php"

```php
class Invite extends Model
{
	protected $fillable = [
	'email', 'token',
	];
}
```

## Yönlendirme yi editle 
"app/routes/web.php"

```php
Route::get('invite', 'InviteController@invite')->name('invite');
Route::post('invite', 'InviteController@process')->name('process');
Route::get('accept/{token}', 'InviteController@accept')->name('accept');
```

## Blade 
"resources/views/invite.blade.php"

```html
<form action="{{ route('invite') }}" method="post">
    {{ csrf_field() }}
    <input type="email" name="email" />
    <button type="submit">Send invite</button>
</form>
```

* Browser http://host/invite  and enter e-mail




## Controller yarat ve editle
`artisan make:controller InviteController`
"app/Http/Controllers/InviteController.php"

```php
<?php

namespace App\Http\Controllers;
use App\User;
use App\Invite;
use App\Mail\InviteCreated;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InviteController extends Controller
{
    // Davet edilecek kişinin e-mailini gir. 
    public function invite()
    {
        return view('invite');
    }

    // Davet gönder.  
    public function process(Request $request)
    {
      // token oluştur
      do { $token = str_random(); } 
      while (Invite::where('token', $token)->first());

      // Invite table ye yaz.
      $invite = Invite::create([
          'email' => $request->get('email'),
          'token' => $token
      ]);

      // Davet Maili gönder.  
      Mail::to($request->get('email'))->send(new InviteCreated($invite));

      return redirect()->back();
    }

    // Davet url den onaylandı ise. Users tablosuna yaz. 
     public function accept($token,Request $request) 
    {

      // $invite e atama yap $invite->email kullan ( if içinde de atama olur!!)
      if (!$invite = Invite::where('token', $token)->first()) {
          abort(404);  // laravel 5.7 de /public/svg/404.svg i kullanır.
      
      //dd($token,request()->all(), $invite->email);  // retrieval test  
      
      // email daha önce verilmişmi User tablosundan kontrol et. 
      if (User::where('email', $invite->email)->first())  { 
          return 'Bu email ile daha önce kayıt olunmuş';
	  // return response()->view('errors.chat_kayit_var'); // özelleştirmek istersen.
      }

      // User tablosuna yaz  
      User::create([
                    'email' => $invite->email,
                    'name' => request('name'),
                    'password' => Hash::make($request->password)                    
                  ]);    
                  
      // User tablosuna email doğrula  kaydını da yaz.          
      User::where('email', $invite->email )->update([
                  'email_verified_at' => now()->toDateTimeString()
                  ]);
            
      $invite->delete(); // Invide tablodaki kaydı sil.

      return 'Good job! Invite accepted!';  
          
    }
}

```

## Mailable sınıfı oluştur
`php artisan make:mail InviteCreated`
"app/Mail/InviteCreated.php"

```php
<?php

namespace App\Mail;
use App\Invite;  // ilave 
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InviteCreated extends Mailable
{
    use Queueable, SerializesModels;
    public $invite;  // ilave
    public function __construct(Invite $invite) // ilave 
    {
      $this->invite = $invite;  // ilave
    }

    public function build()
    {
      return $this->from('ilcine@gmail.com', 'Chat Service' )  
        ->subject('Chat invitation!')
        ->view('emails.invite_msg')
        ->with('email', $this->invite->email);      
    }
}

```
> İstenirse build() te markdown da kullanılır. 
> return $this->markdown('emails.invite_msg')
>	->with('email', $this->invite->email); 
> 

## Kullanım

* Browser de "http//host/invite"  ile davet edilecek kişinin emailini gir.

* Davet edilen kişinin mail adresine gelen mail e "name" ve "şifre" gir ve gönder.




