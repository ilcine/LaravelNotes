# "Page Not Found" Kullanımı 

Bulunmayan linklerde 500 vb html hata kodları yerine tanımlanmış page not found sayfasının döndürülmesi.

_Emrullah İLÇİN_

## app/Exceptions/Handler.php  // edit

```php
  public function render($request, Exception $exception)
  {
  if($this->isHttpException($exception))
    {
      switch ($exception->getStatusCode())
        {
            // not found
            case 404:
		return response()->view('errors.404', [], 404);
            break;
									
            // internal error
            case '500':
		return response()->view('errors.500', [], 500);
            break;
            
        default:
          return $this->renderHttpException($e);
        break;
      }
    }
    else
    {
        return parent::render($request, $exception);
    }
  } 
```
[Diğer Html Status Kodları:](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes)

## resources/views/errors.404.blade.php  // and create other error pages.

```html
@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Chat</div>

                <div class="card-body">
                    Page Not Found!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

```
