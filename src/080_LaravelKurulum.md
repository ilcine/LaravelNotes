# Laravel

Laravel is an open source PHP framework with MVC structure. Object Oriented Programming (OOP) feature is available. It uses Composer, which is a Dependency manager for setup.

_Emrullah İLÇİN_

## ex: Project name, directory, user name, and test environment

Project name: proje1, Directory: $HOME="/home/emr", User name: $USER="emr", public ip: 94.177.187.177, Domain: proje1.ilcin.name.tr, Server: Ubuntu 16.04, php version: 7.2, Web server: Apache 2.4, php framework: Laravel, Js Framework: Vue, CSS Framefork: Bootstrap

## Installation with `Composer`

* `cd $HOME` # go to home directory
* `composer create-project --prefer-dist laravel/laravel proje1`  # add version "ex:5.6" if needed, otherwise the latest version will be installed
* `cd $HOME/proje1` # go to the created directory 
* `php artisan --version`  # test; see version

> laravel creates "package.json", composer.json and  webpack.mix.js config files; Writes packages axios, bootstrap, popper.js, cross-env, jquery", laravel-mix, lodash, vue and etc into the package.json file

> install them if you want; ex: vue and authentication package;  

```
composer require laravel/ui
php artisan ui vue --auth
```
> or this: create project with auth ex: `laravel new project --auth`

## Installation with "Laravel new"

<pre>
   Firstly, install Laravel new structure with composer.

  `cd $HOME` go to home directory
  `composer global require "laravel/installer"` installation created to `$HOME/.config/composer/vendor/bin` directory
  `export PATH=$PATH:$HOME/.config/composer/vendor/bin` # give temporary "path"
  `vi ~/.bashrc` # dosyasına `export PATH=$PATH:$HOME/.config/composer/vendor/bin` # or give permanent "path";
  `laravel --help` # test 
</pre>

* `cd $HOME`  # go to home directory
* `laravel new proje1` # created new project
* `cd $HOME/proje1` # go to project directory
* `php artisan --version`  # installation test; see version

## Ownership and Authorization

:bulb: "www-data" is an apache group on linux; Grant group privileges for "Apache web server" to write; Give write access to "Apache2" log vb files

`sudo chown -R $USER:www-data storage`   
`sudo usermod -a -G www-data $USER`   
`sudo chmod -R 775 storage`  
`sudo usermod -a -G $USER www-data`   # If you need the "storage/logs" authority

## Test (you see laravel default page)

`php artisan serve --host=94.177.187.77 --port=8000` # or host=localhost ,or port any; (press `<ctrl>Z` to exit on Linux console) and See the process with "ps -ef" and kill process PID <br>
    
`http://localhost:8000` or `http://94.177.187.77:8000` # see browser with ip
    
`http://proje1.ilcin.name.tr`  or  `http://proje1.ilcin.name.tr:8000` # see browser with domamin    
 

