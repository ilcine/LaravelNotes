# CONTROLLER

Controller, model den aldığı verileri kontrol eder, düzenler ve View e gönderir.

_Emrullah İLÇİN_

## Controller'i yaratma

* `php artisan make:controller IlkController` # Controller yaratılır.
* `php artisan make:controller IlkController --resource  --model=Task` # örnekte CRUD(yaratma,okuma,güncelleme,silme) için gerekli funksiyonları model ile birlikte şablon olarak yaratır.

> `$HOME/proje1/app/Http/Controllers/IlkController.php`  # Controller'in yazıldığı dosya

## Controller boş örneği (Resource ve modelli)

```php
<?php
namespace App\Http\Controllers;
use App\Task;  /* model adı  */
use Illuminate\Http\Request;
class IlkController extends Controller
{
    
    public function index()
    {
        //
    }
    public function create()
    {
        //
    }
    public function store(Request $request)
    {
        //
    }
    public function show(Task $task)
    {
        //
    }
    public function edit(Task $task)
    {
        //
    }
    public function update(Request $request, Task $task)
    {
        //
    }
    public function destroy(Task $task)
    {
        //
    }
}

```

:bulb: Controller içine yazılacak `//` veya `/* --- */` yorumdur derlemeye girmez.


