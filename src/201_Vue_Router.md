# vue-router

Vue-router recommended for SPA (Single Page Applications)

> _Emrullah İLÇİN_


## 1 - requirement

> laravel 5.8 and above

> vue-router

## 2 - install vue-router

```
cd  `~/{laravel project name}`
npm i vue-router --save
cd  `~/{laravel project name}/resources/js`
```

## 3 - edit  `~/{laravel project name}/resources/js/app.js`
```
require('./bootstrap');
import App from './components/App.vue';
import {router} from './routes';
const app = new Vue({
    el: '#app',
    router,
    components: {App}
});
```

## 4 - edit  `~/{laravel project name}/resources/js/routes.js`

```
import VueRouter from 'vue-router';
import EmrHome from './components/EmrHome'
import EmrAbout from './components/EmrAbout'

export const router = new VueRouter({
    mode: 'history',
		routes: [
			{   
				path: '/',
				name: 'emrhome',
				component: EmrHome,
			},
			{
				path: '/emrabout',
				name: 'emrabout',
				component: EmrAbout,
			},
		]
});
```

## 5 - edit  `~/{laravel project name}/resources/js/bootstrap.js` add 4 lines below

```
import Vue from 'vue';
window.Vue   = Vue;

import VueRouter from 'vue-router';
window.Vue.use(VueRouter);
```

## 6 - edit edit  `~/{laravel project name}/resources/js/components/App.vue`

```
 <template>
	<div>
		<h1>Vue Router Demo App</h1>
		<p>
			<router-link :to="{ name: 'emrabout' }">emr ab Home</router-link>
			<router-link :to="{ name: 'emrhome' }">emr Hello World</router-link>
		</p>
		<div class="container">
			<router-view></router-view>
		</div>
	</div>
</template>
<script>
    export default {}
</script>
```

## 7 - edit edit  `~/{laravel project name}/resources/js/components/EmrAbout.vue`

```
<template>
    <div>
	    Content for Emr About Page
    </div>
</template>

<script>
    export default {
        data(){
            return {
            }
        }
    }
</script>
```

## 8 - edit  `~/{laravel project name}/resources/js/components/EmrHome.vue`

```
<template>
    <div>
	    Content for Emr Home Page
    </div>
</template>

<script>
    export default {
        data(){
            return {
            }
        }
    }
</script>
```

## 8 - Run 

```
npm run dev
```

## 9 - Router line

echo "Route::get('/vuerouter', function () { return view('vuerouter'); });" >> `~/{laravel project name}/routes/web.php


## 9 -  edit  `~/{laravel project name}/resources/view/vuerouter.blade.php`

```
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title></title>
        <!-- Styles -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="app">
            <App/>
        </div>
        <script src="{{ asset('spa/js/app.js') }}"></script>
    </body>
</html>
```
## 9 - Browser

```
http://{ip or url}/vuerouter  // Write the necessary definitions to Apache2
```

or 
```
php artisan serve --host={ ip } --port=8000  // or --host=localhost

http://{ip}:8000/vuerouter
```


