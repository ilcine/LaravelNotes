# VUE 

VUE destekleyici kütüphanelerle birlikte, gelişmiş Tek Sayfa(SPA) Uygulamalarını güçlendirebilme yeteneğine sahip javascript framework(Çerçeve) dir. frontend(Ön yüz)geliştiricileri için HTML,CSS ve Javascript arasındaki ilişkileri çift yönlü kullanır, yeni yaratılacak componentler içine özel attribute ler tanımlar, VUE hızlı ve basit kullanım yapısıyla Angular,React vb nin önüne geçer.

_Emrullah İLÇİN_

## VUE Tanımlamaları
Laravel projesi içinde vue2 ve bootstrap4 kurulu olarak gelir. 
js tanımları, `resources/assets/js/app.js` ye; css ve scss  tanımları `resources/assets/sass/app.scss` a yapılır.

## Component 
html için yeni component tanımlanır, ör: `<emr> </emr>` component'in tanımlanması

* `./resources/assets/js/app.js` içine `Vue.component('emr', require('./components/emr.vue'));` ile emr componentinin yolu tanımlanır.

```js
require('./bootstrap');
window.Vue = require('vue');
Vue.component('emr', require('./components/emr.vue'));
const app = new Vue({
    el: '#app'
});
```

* Örnek `emr.vue` component i `./resources/assets/js/components/emr.vue` içine tanımlanır.

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="card text-center">
          <div class="card-header">
            emr Component Test OK.
          </div>
        </div> 
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    mounted() {
      console.log('Component mounted.')
      }
    }
</script>

```

* npm Çalıştırılır.
`npm run dev` komutu ile componentler derlenir, hatalı işlemler listelenir, Sonuç ok. ise alttaki gibi bir çıktı alınır, npm  `public/js/app.js` ve `public/css/app.css` leri oluşturur, yaratılan bu js ve css ler html'de kullanılacaktır. Derleme sonucu alttaki gibi bir çıktı alınır:

```
DONE  Compiled successfully in 7885ms           10:14:57

Asset        Size        Chunks                Chunk Names
------------ ----------- --------------------- ------------
/js/app.js   1.36 MB       0 [emitted]  [big]  /js/app
/css/app.css 187 kB        0 [emitted]         /js/app
```
> Diğer derleme yöntemleri `npm run production` , `npm run watch`. 

:bulb: `component`, `app.js`, `app.scss` lerde değişiklikler yapıldığında `npm run dev` çalıştırılmalı, eğer sürekli değişiklik yapılıyorsa `npm run watch` komutu editlenen sayfaları tanır ve o an derlemeye başlar. Yazılım geliştirme sırasında `npm run watch` çokça kullanılır.

* route tanımı: `./routes/web.php` ye yapılır. örnek `http://proje1.ilcin.name.tr/vuetest` sayfa tanımı. 

`echo "Route::get('/vuetest', function () { return view('vuetest'); });" >> ./routes/web.php`

Sonuç:

```js
Route::get('/vuetest', 
   function () { 
     return view('vuetest'); 
   });
```

* Layouts tanımı:
`resources/views/layouts/appVue.blade.php` dosyasına yapılır.

```html
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <!-- Meta -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}" />
	<!-- Styles -->
	<link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

  <!-- Vue için id tanımı -->
  <div id="app">
        @yield('content')
   </div>

  <!-- Scripts -->
  <script src="{{ asset('js/app.js') }}" type="text/javascript"   ></script>	
</body>
</html>
```
:bulb: Vue Componentler için id=app tanımı yapılır.  

* blade tanımı: `resources/views/vuetest.blade.php` sayfasına yapılır.

echo -e "@extends('layouts.appBasit') \n@section('content') \n<emr></emr> \n@endsection" >  ./resources/views/vuetest.blade.php

Sonuç:

```html
@extends('layouts.appVue')
@section('content')
<emr></emr>
@endsection
```

:bulb: Örnekteki dosya yaratmaları linux un echo komutu ile basit olarak yaratıldı, kullanıcılar herhangibir editör kullanarak tanımları yapabilir.

## emr Componet Test: 

http://proje1.ilcin.name.tr/vuetest
`emr Component Test OK.` geliyorsa component ok.

 

