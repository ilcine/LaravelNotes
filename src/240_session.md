# Laravel Session
Oturum yaratma

> _Emrullah İLÇİN_

## Session destekleri (The default session is "file")

Supported: "file", "cookie", "database", "apc", "memcached", "redis", "array"

## Change .env config file 

Support for "Database Session"; Use "database" instead of "file"; Replace "file" with "database" on `.env` file

`SESSION_DRIVER=database`

## Create SQL session table; "migrate" operation writes to SQL 

```
php artisan session:table
php artisan migrate
```

* "php artisan session:table" command create `database/migrations/XXXX` file

* "php artisan migrate" command created this table structure

```sql
CREATE TABLE `sessions` (
  `id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `ip_address` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_agent` text COLLATE utf8mb4_unicode_ci,
  `payload` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
```
