# Laravel de date ve time   

Laravel date ve time işlemlerinde "Carbon" uygulaması kullanılır. Uygulamada Carbon u kullanmak için controller de `use Carbon\Carbon;` kullan.

> _Emrullah İLÇİN_

## Varsayılan Zaman damgası 
`Y-m-d H:i:s` tir TR ye değiştirmek için `config/app.php` dosyasında değişiklik yap.
```
'timezone' => env('APP_TIMEZONE', 'Europe/Istanbul'),
'locale' => 'tr',
```

## Controller de kullanmık için 
`use Carbon\Carbon;`

* `now()` or `Carbon::now();` # ör:2018-10-04 11:59:29.930858 Europe/Istanbul (+03:00)
* `now()->timestamp`  # epoch formatında şu an ör: 1538643395 
* `now()->toDateTimeString();` # ör: "2018-10-04 12:00:36" // Tarih ve saat 
* 'now()->addMonth()->format('d-m-Y');` # bir ay sonra
* `Carbon::yesterday();` # dünkü tarih # ör: 2018-10-03 00:00:00.0 Europe/Istanbul (+03:00)

```
$dt = Carbon::create(2018, 10, 4, 0);
$now = $dt->addDays(7);
dd($now); // controller de içeriği gösterir "print_r()", "var_dump()" gibi  
```
İlgili günde 7 gün koy ör: date: 2018-10-11 00:00:00.0 Europe/Istanbul (+03:00)

## Blade de 

`<input type="date" class="form-control date" value="{!! $now  !!}" >`

`{{ date('d-m-Y H:i:s', strtotime($task->updated_at))  }}`

`{{ Carbon\Carbon::parse($task->created_at)->format('d-m-Y i') }}`



