# ROUTING
Laravel'de ilk işlem routing ile başlar, tarayıcıların URL'sine yazılacak yönlendirme ile routing işlemi direkt Controller'e veya View'e olabilir. 
> _Emrullah İLÇİN_

## Routing için projenın routes dizini altında web.php kullanılır.
* `cd $HOME/proje1/routes/web.php`

## web.php 
Tüm yönlendirmelerin yazıldığı dosya. Yeni yönlendirmeler varsa dosyanın sonuna ilave edilir. `/` yönlendirmesi kök anlamındadır, projede ilk olarak açılacak web sayfasının yolunu belirler, view ile yönlendirilir, welcome dosyasına html,js,css kodları yazılır. welcome dosyasının adı laravelde welcome.blade.php olarak tanımlanır.  

* web.php'nin Default hali

```php
Route::get('/', function () {
    return view('welcome');
});
```

## Direkt view sayfasına yönlendirme
Bu tür yönlendirmede sayfa direkt view e gider (contoller'e uğramaz),

```php
Route::get('/ilksaya', function () {
    return view('ilksayfa');
});
```

## Controller sayfasına yönlendirme
Sayfa Controlle' e gider,

    Route::get('/ilksayfa', 'IlkController@ilksayfa');   

> :bulb: `@ilksayfa` ifadesi contorollerdeki fonksiyonu ifade eder istenirse burada başka ifade de kullanılır. IlkContrloller Sayfası alttaki gibi olabilir. Controller sayfasında sayfa açılmadan önce doğrulama, db tablo değerlerini atama gibi işlemler yapılır.

```php
  <?php
  namespace App\Http\Controllers;
  use Illuminate\Http\Request;
  class IlkController extends Controller
  {
   public function ilksayfa()
   {
     return view('ilksayfa');
   }
  }
```

## Resource kullanımı

```php
// resource kullanımı 
Route::resource('users', 'UserController');
// işlem in etkisi
Route::get('users',['as'=>'users.index','uses'=>'UserController@index']);
Route::post('users/create',['as'=>'users.store','uses'=>'UserController@store']);
Route::get('users/edit/{id}',['as'=>'users.edit','uses'=>'UserController@edit']);
Route::patch('users/{id}',['as'=>'users.update','uses'=>'UserController@update']);
Route::delete('users/{id}',['as'=>'users.destroy','uses'=>'UserController@destroy']);
Route::get('users/{id}',['as'=>'users.show','uses'=>'UserController@show']);
```

## Resource Only ve except kullanımı

```php
Route::resource('photos', 'PhotoController', ['only' => [ 'index', 'show']]);
Route::resource('photos', 'PhotoController', ['except' => [ 'create', 'store', 'update', 'destroy']]);
```

## Auth

`php artisan make:auth` ile Laravel'in içindeki kimlik doğrulama yapısını aktif hale getirir. 

* `./routes/web.php` de `Auth::routes();` oluşur. Açıklamalı yazılımı alttaki gibidir.

```php
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');

// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
```

* `./ls app/Http/Controllers/Auth/` dizini altında control'ler oluşur.

```php
ForgotPasswordController.php  
RegisterController.php
LoginController.php           
ResetPasswordController.php
```

* `resources/views/auth/` dizini altında view'ler oluşur

```php
login.blade.php     
passwords/email.blade.php  
passwords/reset.blade.php          
register.blade.php
```

* `resources/views/layouts/app.blade.php` şablonu oluşur.

* `resources/views/home.blade.php` oluşur.
 
 
* Doğrulama sonrası view altında `home.blade.php` sayfası açılır, 

Farklı sayfa açılması için.
`RegisterController.php` ve `LoginController.php` dosyalarındaki  `protected $redirectedTo = '/home';` da bulunun home yerine  `resources/views/XXX.blade.php` de ifade edilen xxx sayfa adı yazılır.

* Auth test için http://proje1.ilcin.name.tr/login.php 

## Test
route lerin tamamı listelenir.

`php artisan route:list`

