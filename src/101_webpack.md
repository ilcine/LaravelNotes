# Webpack 

> Emrullah İLÇİN

## Install

Go to laravel project in `$home/proje1` directory

```
npm init -y
npm install laravel-mix --save-dev
cp -r node_modules/laravel-mix/setup/webpack.mix.js ./
```

## Change scripts block in package.json
```
{
"private": true,
"scripts": {
  "dev": "npm run development",
  "development": "cross-env NODE_ENV=development node_modules/webpack/bin/webpack.js --progress --hide-modules --config =node_modules/laravel-mix/setup/webpack.config.js",
  "watch": "npm run development -- --watch",
  "watch-poll": "npm run watch -- --watch-poll",
   "hot": "cross-env NODE_ENV=development node_modules/webpack-dev-server/bin/webpack-dev-server.js --inline --hot --dis
able-host-check --config=node_modules/laravel-mix/setup/webpack.config.js",
   "prod": "npm run production",
  "production": "cross-env NODE_ENV=production node_modules/webpack/bin/webpack.js --no-progress --hide-modules --confi
g=node_modules/laravel-mix/setup/webpack.config.js"
    }    
```

* Add this block for Laravel as well;
```
,
"devDependencies": {
	"axios": "^0.19",
	"bootstrap": "^4.0.0",
	"cross-env": "^7.0",
	"jquery": "^3.2",
	"laravel-mix": "^5.0.1",
	"lodash": "^4.17.19",
	"popper.js": "^1.12",
	"resolve-url-loader": "^2.3.1",
	"sass": "^1.20.1",
	"sass-loader": "^8.0.0",
	"vue": "^2.5.17",
	"vue-template-compiler": "^2.6.10"
}
``` 

## Add or change in webpack.mix.js file

```
let mix = require('laravel-mix');
    mix.js('src/app.js', 'dist/')
       .sass('src/app.scss', 'dist/');
```

## run

```
npm run dev # Compiler js and css
npm run watch # Detects the change made in Js and compiler it at that time
npm run production # executed when the project is completed, shrinks the js file;
```

> Compiled js and css files are written to the "public/js/app.js" and "public/css/app.css" files
