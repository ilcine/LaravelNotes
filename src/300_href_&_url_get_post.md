# Laravel ve Blade için get/post

Laravel Controller ve Blade arasında veri taşımak için get ve post kullanımı

_Emrullah İLÇİN_

## Ex 1:

* "routers/web.php"

```php
Route::get('invite', 'InviteController@invite')->name('invite');
```

* "app/Http/Controllers/InviteController.php" 

```php
class InviteController extends Controller
{
  public function invite()
  {
   return view('invite');
  }
}
```

* "invite.blade.php"
```php
<form action="{{ route('invite') }}" method="post">
    {{ csrf_field() }}
    <input type="name" name="name" />
    <button type="submit">Send invite</button>
</form>
```
* "Browser"

```http
http://host.ilcin.name.tr/invite
```
insert name and submit

* "routers/web.php"
```php
Route::post('invite', 'InviteController@process')->name('process');
```

* "app/Http/Controllers/InviteController.php" 

```php
public function process(Request $request)
{
    dd(request()->all()); // display name 
    return redirect()->back();
}
```

## Ex 2:

* Router
```php
Route::get('accept/{token}', 'InviteController@accept')->name('accept');
```

* Browser
```http
http://host.ilcin.name.tr/accept/Emrullah İLÇİN
```

*  Controller
```php
public function accept($token) 
{
  dd($token); //* display Emrullah İLÇİN
}
```

## Ex 3

* Router

```php
Route::get('accept/{token}', 'InviteController@accept')->name('accept');
```

* Browser

```http
http://host.ilcin.name.tr/accept/Bursa?name="Emrullah İLÇİN"
```

*  Controller
```php
public function accept($token,Request $request)
{
  dd($token,request()->all() );	 //* display token: "Bursa"  and request : "Emrullah İLÇİN" 
}
```

## Ex 4

* Router
```php
Route::get('accept/{sehir}{name}', 'InviteController@accept')->name('accept');
```

* Browser

```http
http://host.ilcin.name.tr/accept/Yeşil Bursa/Emrullah İLÇİN
```

*  Controller
```php
public function accept($sehir,$name)
{
  dd($sehir,$name );	 //* display sehir: "Yeşil Bursa"  and name : "Emrullah İLÇİN" 
}
```
