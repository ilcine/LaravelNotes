<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
	protected $table = 'tasks';
	protected $primaryKey = 'id';
	public  $incrementing = true;
  protected $fillable = [ 'name'];
}
