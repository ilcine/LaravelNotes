<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/ilksayfa', function () { return view('ilksayfa'); });

 Route::get('/TaskListesi',		'TaskController@TaskListesi');
Route::post('/TaskListesi',		'TaskController@TaskAra');

 Route::get('/TaskDetay/{id}',		'TaskController@TaskDetay');	
Route::post('/TaskDetay/{id}',		'TaskController@TaskDetayGuncelle');

 Route::get('/TaskYeniKayit/{id}',		'TaskController@TaskYeniKayit');
Route::post('/TaskYeniKayit/{id}',		'TaskController@TaskYeniKayitYaz');	

 Route::get('/TaskSil/{id}',		'TaskController@TaskSilinecek');
Route::post('/TaskSil/{id}',		'TaskController@TaskSil');
