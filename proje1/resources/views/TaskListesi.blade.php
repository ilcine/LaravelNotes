@extends('layouts.appBasit')
@section('content')
<h2> Liste </h2>
<div>
	@if ($tasks->isEmpty()) <p> Kayıt Yok.</p> 
		@else
		  @if (session('status'))	
		  <p> {{ session('status') }} </p>
		  @endif
</div>
	
	<!-- Arama Bölümü -->
	<table style="width:40%">
	  <tr>			
			<td>
				<form method="post">
					<input type="hidden" name="_token" value="{!! csrf_token() !!}">
					<input type="text" name="txtAra" value="">
			</td>
			<td>			
					<button name="btn" value="ara" class="ghost-button" >Ara</button>	 
				</form>
			</td>
		</tr>
	</table>	
	<br>	
	
	<!-- listeleme Bölümü -->	
	<table style="width:40%">
		<thead>
			<tr>
				<th style="width:10%" >id</th>
				<th style="width:30%" >Adı</th> 
				<th>Detay</th>	
				<th>Sil</th>
			</tr>
		</thead>
		
		<tbody>
			
			@foreach($tasks as $key => $task)
			<tr>		
				<td> {{ $task->id }} </td>
				<td> {!! $task->name !!} </td>					
				<td> <a href="{!! action('TaskController@TaskDetay', encrypt($task->id)) !!}'"  class="ghost-button" > Detay </a> </td>
				<td> <a href="{!! action('TaskController@TaskSilinecek', encrypt($task->id)) !!}" class="ghost-button"  > Sil </a> </td>
			</tr>	
			@endforeach
			
		</tbody>
	</table>
    
	@endif		       
@endsection


