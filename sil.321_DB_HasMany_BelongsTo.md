# HasMany and BelongsTo

Ex 1: Schema MuhYev
```
Schema::create('muh_yev', function (Blueprint $table) {
  $table->bigIncrements('id');
  $table->unsignedBigInteger('muh_pla_id');
  $table->string('yev_name',80);
});
DB::table('muh_yev')->insert(array('id' => '1', 'muh_pla_id' => '1','yev_name' => 'yev first'));
DB::table('muh_yev')->insert(array('id' => '2', 'muh_pla_id' => '1','yev_name' => 'yev second'));
```

Model MuhPla
```
Schema::create('muh_pla', function (Blueprint $table) {
  $table->bigIncrements('id');
  $table->integer('kod');
  $table->string('pla_name',80);
  

```

Model MuhYev and function muhpla_new()  // 1.e bir.

Model: `return $this->hasMany('MuhPla', 'id', 'muh_pla_id')->select('muh_pla.id','muh_pla.p_adi')->where('muh_pla.id',1);`

Or this Model: `return $this->belongsTo('MuhPla',  'muh_pla_id')->select('muh_pla.id','muh_pla.p_adi')->where('muh_pla.id',1);`

Routes: `return MuhYev::where('id',1)->select('id','muh_pla_id',yev_name')->with('muh_pla_new')->get();`

Return:

```
[{"id":1,"muh_pla_id":1,"yev_name":"yev first",
  "muh_pla_new":[{"id":1,"pla_name":"pla name"}]
}]
```

-------

Model MuhYev and function muhpla_new()  // 1.e çok.

Model: `return $this->hasMany('Emr\Muh\MuhPla', 'kod')->select('muh_pla.id','muh_pla.p_adi','kod','pla_name');`

Routes: `return MuhYev::where('id',1)->select('id','muh_pla_id','acik')->with('muh_pla_new')->get();`

Return:

```
[{"id":1,"muh_pla_id":1,"acik":"yev first",
  "muh_pla_new":[{"id":1, "kod":"1","pla_name":"111"},
                 {"id":2, "kod":"1","pla_name":"222"},
                 {"id":3, "kod":"1","pla_name":"333"}]
                 }]
 ```                


